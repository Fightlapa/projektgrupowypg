# Automatic test checker based on DNN
 
Project developed as a Project Group PG

## How to run on docker.
```
docker build -t <imagename> .
docker run -p <local_ftp_port>:21 -p <local_web_port>:8000 -v /log:/log <imagename>
```

## How to run tests.

```
python -m unittest <file path>
```

### Example

```
python -m unittest UnitTests.FreakTests
python -m unittest UnitTests.Data.ImageStateUnitTests
```



