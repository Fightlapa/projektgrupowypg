import logging
import os
import csv
import io
import datetime
from multiprocessing import Process

from django.conf import settings
from django.contrib import messages
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView

from Src.MainController import process_pdf_package, process_pdf_single_test
from Src.DataBaseController import update_test
from Src.AppraisalsController import appraise_test
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpRequest, HttpResponse, FileResponse, HttpResponseForbidden, HttpResponseRedirect, request
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Q
from Src.TestEvaluator import create_empty_package
from main.filters import PackageFilter, TestFilter

from main.models import User, Package, Test, ImageContainer, AnswerContainer
from main.forms import PackageCreateForm, TestCreateForm, PackageUpdateForm, TestUpdateForm

logger = logging.getLogger('exams')


class package_create_view(LoginRequiredMixin, CreateView):
    model = Package
    form_class = PackageCreateForm
    success_url = '/package/'

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data(**kwargs)
        context.update({'title': "Upload filled exams"})
        return context

    def form_valid(self, form):
        filename = os.path.join(settings.BASE_DIR, 'exam_pdfs', self.request.FILES['pdf'].name)
        file_path = os.path.join(settings.BASE_DIR, 'exam_pdfs')
        with open(filename, 'wb+') as destination:
            for chunk in self.request.FILES['pdf'].chunks():
                destination.write(chunk)
        src = os.path.join(file_path, self.request.FILES['pdf'].name)
        package = create_empty_package(src, package_name=form.cleaned_data['name'])
        Package.objects.filter(pk=package.id).update(user=self.request.user)
        Package.objects.filter(pk=package.id).update(score_mode=form.cleaned_data['score_mode'])
        package.score_mode = form.cleaned_data['score_mode']
        processing_process = Process(target=process_pdf_package, args=[self.request.FILES['pdf'].name, src, package])
        processing_process.start()
        self.success_url += str(package.id)
        return super().form_valid(form)


def reappraise_package(package):
    ref_tests = Test.objects.filter(package=package.id, is_reference=True)
    for ref_test in ref_tests:
        ref_test.images = []
        for image in ImageContainer.objects.filter(test=ref_test).iterator():
            image.answers = AnswerContainer.objects.filter(img=image)
            ref_test.images.append(image)
    package.reference_tests = ref_tests
    for test in Test.objects.filter(package=package.id, is_reference=False).iterator():
        reappraise_test(package, test)


def reappraise_test(package, test):
    test.images = []
    for image in ImageContainer.objects.filter(test=test).iterator():
        image.answers = AnswerContainer.objects.filter(img=image)
        test.images.append(image)
    appraise_test(test, package)
    update_test(test)


def package_reevaluate(request, pk):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    package = Package.objects.get(pk=pk)
    reappraise_package(package)

    success_url = '/package/' + str(pk)
    next_page = request.POST.get('next', success_url)
    return HttpResponseRedirect(next_page)

def test_reevaluate(request, pk):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    test = Test.objects.get(pk=pk)
    backend_package = test.package.cast_to_backend_object()
    reappraise_test(backend_package, test)

    success_url = '/test/' + str(pk)
    next_page = request.POST.get('next', success_url)
    return HttpResponseRedirect(next_page)

def answer_update(request, image_id, answer_id, letter, value):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    image = ImageContainer.objects.get(id=image_id)
    answer = AnswerContainer.objects.get(img=image, answer_id=answer_id)
    current_value = set(answer.evaluated_value)
    if value == 'true':
        current_value.add(letter)
    else:
        current_value.remove(letter)
    AnswerContainer.objects.filter(pk=answer.id).update(evaluated_value=''.join(str(s) for s in sorted(current_value)))
    return HttpResponse('')


def package_raport(request, pk):
    if request.user.is_authenticated is False or (Package.objects.get(pk=pk).user != request.user and Package.objects.get(pk=pk).user != None):
        return HttpResponseForbidden()
    package = Package.objects.get(pk=pk)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s_raport.csv"' % package.name
    writer = csv.writer(response, delimiter=',')
    writer.writerow(['student_index', 'acquired_points'])
    for test in Test.objects.filter(package=package, is_reference=False):
        writer.writerow([test.student_index, test.acquired_points])
    return response

def package_pdf(request, pk):
    if request.user.is_authenticated is False or (Package.objects.get(pk=pk).user != request.user and Package.objects.get(pk=pk).user != None):
        return HttpResponseForbidden()
    package = Package.objects.get(pk=pk)
    buffer = io.BytesIO()
    with open(os.path.join(package.folder_path, "test.pdf"), "rb") as pdf:
        buffer.write(pdf.read())
        buffer.seek(io.SEEK_SET)
        return FileResponse(buffer, as_attachment=True, filename=package.name + '.pdf')


def template_download_view(request):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'main/test_template.html'
    )


def template_download(request):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    buffer = io.BytesIO()
    with open("./main/static/template.pdf", "rb") as template_pdf:
        buffer.write(template_pdf.read())
        buffer.seek(io.SEEK_SET)
        return FileResponse(buffer, as_attachment=True, filename='template.pdf')


class test_list_view(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Package
    success_url = '/package/'
    paginate_by = 15

    def get_queryset(self):
        return Test.objects.filter(package=self.kwargs['package'], is_reference=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['package'] = Package.objects.get(pk=self.kwargs['package'])
        try:
            context['ref_tests'] = Test.objects.filter(is_reference=True, package=self.kwargs['package'])
        except Test.DoesNotExist:
            context['ref_tests'] = None
        filtered_qs = TestFilter(self.request.GET, queryset=self.get_queryset()).qs.order_by('student_index')
        context['filter'] = TestFilter(self.request.GET, queryset=filtered_qs)
        context['paginator'] = Paginator(filtered_qs, self.paginate_by)
        page = self.request.GET.get('page')
        if page is None:
            page = 1
        context['page_obj'] = context['paginator'].page(page)
        context['update_form'] = PackageUpdateForm(initial={'name': context['package'].name,
                                                            'user': context['package'].user,
                                                            'score_mode': context['package'].score_mode,
                                                            'allow_negative': context['package'].allow_negative})
        return context

    # If authorized
    def test_func(self):
        # if self.request.user.has_perm('main.create_exam'):
        if self.request.user == Package.objects.get(pk=self.kwargs['package']).user or Package.objects.get(pk=self.kwargs['package']).user == None:
            return True
        return False

    def post(self, request, package, slug=None):
        package = Package.objects.get(pk=self.kwargs['package'])
        if len(request.POST) == 1:
            package.delete()
            # get_object_or_404(package.model_class(), pk=pk).delete()
            response = redirect('main-packages')
            return response
        else:
            form = PackageUpdateForm(request.POST)
            if form.is_valid():
                if package.name != form.cleaned_data['name']:
                    Package.objects.filter(pk=package.id).update(name=form.cleaned_data['name'])
                if package.user != form.cleaned_data['user']:
                    Package.objects.filter(pk=package.id).update(user=form.cleaned_data['user'])
                if package.score_mode != form.cleaned_data['score_mode']:
                    Package.objects.filter(pk=package.id).update(score_mode=form.cleaned_data['score_mode'])
                    package.score_mode = form.cleaned_data['score_mode']
                    reappraise_package(package)
                if package.allow_negative != form.cleaned_data['allow_negative']:
                    Package.objects.filter(pk=package.id).update(allow_negative=form.cleaned_data['allow_negative'])
                    package.allow_negative = form.cleaned_data['allow_negative']
                    reappraise_package(package)
            next_page = request.POST.get('next', self.success_url + str(package.id))
            return HttpResponseRedirect(next_page)


class packages_list_view(LoginRequiredMixin, ListView):
    model = Package
    paginate_by = 10

    def get_queryset(self):
        return Package.objects.filter(Q(user=self.request.user) | Q(user=None)).order_by('-date_created')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        filtered_qs = PackageFilter(self.request.GET, self.get_queryset()).qs.order_by('-date_created')
        context['paginator'] = Paginator(filtered_qs, self.paginate_by)
        context['filter'] = PackageFilter(self.request.GET, queryset=filtered_qs)
        page = self.request.GET.get('page')
        if page is None:
            page = 1
        context['page_obj'] = context['paginator'].page(page)
        return context

class test_detail_view(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Test
    context_object_name = 'test'
    success_url = '/test/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        images = []
        for image in ImageContainer.objects.filter(test=self.object):
            image_dict = {}
            image_dict['answers'] = AnswerContainer.objects.filter(img=image)
            image_dict['image'] = image
            images.append(image_dict)
        context['images'] = images
        context['possible_answers'] = ['A','B','C','D']
        context['test_update_form'] = TestUpdateForm(initial={'student_index': self.object.student_index, 'test_group': self.object.test_group, 'is_reference': self.object.is_reference})
        context['ref_tests_groups'] = [x.test_group for x in Test.objects.filter(is_reference=True, package=self.object.package)]
        # context['package_name'] = ImageContainer.objects.get(pk=self.object.package).name
        return context

    # If authorized
    def test_func(self):
        # if self.request.user.has_perm('main.create_exam'):
        if self.request.user == Test.objects.get(pk=self.kwargs['pk']).package.user or Test.objects.get(pk=self.kwargs['pk']).package.user == None:
            return True
        return False

    def post(self, request, pk, slug=None):
        if len(request.POST) == 1:
            response = redirect('main-package-detail', package=self.get_object().package.id)
            Test.objects.get(pk=self.get_object().id).delete()
            return response
        else:
            this_test = self.get_object()
            form = TestUpdateForm(request.POST)
            if form.is_valid():
                if this_test.student_index != form.cleaned_data['student_index']:
                    Test.objects.filter(pk=this_test.id).update(student_index=form.cleaned_data['student_index'])
                if this_test.test_group != form.cleaned_data['test_group']:
                    Test.objects.filter(pk=this_test.id).update(test_group=form.cleaned_data['test_group'])
                if this_test.is_reference != form.cleaned_data['is_reference']:
                    if form.cleaned_data['is_reference'] is True and form.cleaned_data['test_group'] in [x.test_group for x in Test.objects.filter(is_reference=True, package=Test.objects.get(id=pk).package)]:
                        messages.error(request, "Reference test already exists for group " + form.cleaned_data['test_group'])
                    else:
                        Test.objects.filter(pk=this_test.id).update(is_reference=form.cleaned_data['is_reference'])
            self.success_url += str(pk)
            next_page = request.POST.get('next', self.success_url)
            return HttpResponseRedirect(next_page)


class test_create_view(LoginRequiredMixin, CreateView):
    model = Test
    form_class = TestCreateForm
    success_url = '/package/'

    def form_valid(self, form, **kwargs):
        filename = os.path.join(settings.BASE_DIR, 'exam_pdfs', self.request.FILES['pdf'].name)
        file_path = os.path.join(settings.BASE_DIR, 'exam_pdfs')
        with open(filename, 'wb+') as destination:
            for chunk in self.request.FILES['pdf'].chunks():
                destination.write(chunk)
        package = Package.objects.get(pk=self.kwargs['pk'])
        package = package.cast_to_backend_object()
        src = os.path.join(file_path, self.request.FILES['pdf'].name)
        processing_process = Process(target=process_pdf_single_test, args=[src, package])
        processing_process.start()

        self.success_url += str(package.id)
        return super().form_valid(form)
