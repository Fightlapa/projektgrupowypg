from main.models import Package

def one_time_startup():
    from django.db import connection
    cursor = connection.cursor()
    if not cursor:
        raise Exception
    if 'main_package' in connection.introspection.table_names(cursor):
        packages = Package.objects.filter(status='processing')
        for package in packages:
            Package.objects.filter(pk=package.id).update(status='finished')
