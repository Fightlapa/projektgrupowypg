from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Package, Test, AnswerContainer, ImageContainer, AccessToken
from .forms import UserRegisterForm


class CustomUserAdmin(UserAdmin):
    add_form = UserRegisterForm
    model = User
    list_display = ['email', 'username',]


admin.site.register(User, CustomUserAdmin)
admin.site.register(Package)
admin.site.register(Test)
admin.site.register(AnswerContainer)
admin.site.register(ImageContainer)
admin.site.register(AccessToken)

# Register your models here.
