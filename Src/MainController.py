
import os

from Src.DataBaseController import change_package_status
from Src.TestEvaluator import evaluate_tests, create_package_from, get_pdf_pages, save_or_append_pdf

MODULE_NAME = 'MainController'

if os.name == 'posix':
    LOG_FILE = '/log/Tester.log'
else:
    LOG_FILE = 'app.log'

import logging
import traceback
import time
from logging.handlers import TimedRotatingFileHandler

# create logger with 'spam_application'
logger = logging.getLogger('exams')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = TimedRotatingFileHandler(LOG_FILE, when='D')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(filename)s - %(asctime)s - %(levelname)s - %(funcName)s - %(message)s',
                              "%Y-%m-%d %H:%M:%S")
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

def process_pdf_package(filename, src, package):
    try:
        change_package_status(package, 'processing')
        start = time.time()
        save_or_append_pdf(src, package)
        create_package_from(src, package)
        end = time.time()
        logger.info('Processed in {}'.format(str(end - start)))
        change_package_status(package, 'finished')
    except Exception as e:
        tb = traceback.format_exc()
        logger.error('Failed to proceed {} reason: {}'.format(filename, str(e)))
        logger.error(tb)
        change_package_status(package, 'error', str(e)[:100])


def process_pdf_single_test(src, package):
    try:
        change_package_status(package, 'processing')
        start = time.time()
        save_or_append_pdf(src, package)
        pages = get_pdf_pages(src)
        evaluate_tests(package, pages)
        end = time.time()
        logger.info('Processed in {}'.format(str(end - start)))
        change_package_status(package, 'finished')
    except Exception as e:
        print(str(e))
        logger.error('Failed to proceed {} reason: {}'.format(src, str(e)))
        change_package_status(package, 'error', str(e)[:100])
