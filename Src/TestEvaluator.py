import logging
import shutil
import subprocess

import cv2
import os
import tempfile
import pathlib
import uuid
import traceback
from pdf2image import convert_from_path

from Src.AppraisalsController import appraise_test
from Src.DataBaseController import save_package, save_test, change_test_to_ref_test
from Src.Data.Test import Test
from Src.Data.ImageContainer import ImageContainer
from Src.Preprocessing.data_extractor import generate_empty_answers
from Src.ImgNormalization import normalize_image
from Src.Data.Package import Package
import Src.Net.DNNApi as DNNApi
from unidecode import unidecode
import numpy as np

MODULE_NAME = "TestEvaluator"

NAME = 'pdf_page'

if os.name == 'posix':
    DATA_DIRECTORY = "/data"
else:
    DATA_DIRECTORY = os.path.join(str(pathlib.Path.home()), 'data')
NUMBER_OF_POSSIBLE_ANSWERS = 4  # a,b,c,d
logger = logging.getLogger('exams')


def create_empty_package(src, dst=tempfile.TemporaryDirectory().name, package_name: str = None):
    package = Package()
    if not os.path.exists(dst):
        os.mkdir(dst)
    logger.info('File: {} process...'.format(src))
    filename = os.path.basename(src)
    if package_name is None:
        package.Name = os.path.splitext(filename)[0]
    else:
        package.Name = package_name

    set_final_directory(package)
    save_package(package)

    return package


def create_package_from(src, package):
    logger.info('Creating package')
    logger.info('Loading pages...')
    pages = get_pdf_pages(src)
    logger.info('Loading pages... Done')
    assert len(pages) > 0
    DNNApi_instance = DNNApi.DNNApi()
    package.reference_tests = []

    previous_test = None
    while len(pages) > 0:
        current_test = get_next_valid_test(package, pages, DNNApi_instance)
        if any(current_test.test_group == ref_test.test_group for ref_test in package.reference_tests) or current_test.test_group == 'X':
            break
        change_test_to_ref_test(current_test)
        package.reference_tests.append(current_test)
        previous_test = current_test

    package.tests = evaluate_tests(package, pages)


def get_next_valid_test(package, pages, DNNApi_instance):
    current_test = None
    while not current_test and len(pages) > 0:
        try:
            logger.info('Processing page. {} left.'.format(len(pages)))
            current_test = evaluate_test(package, pages[0], DNNApi_instance)
        except Exception as e:
            logger.error(e)
            pages.remove(pages[0])
            continue
        pages.remove(pages[0])
        return current_test
    if current_test is None:
        raise Exception("No valida pages found in pdf")

def get_pdf_pages(src):
    return convert_from_path(src, fmt='JPEG')

def save_or_append_pdf(src, package):
    pdf_path = os.path.join(package.folder_path, 'test.pdf')
    if os.path.exists(pdf_path):
        temp_pdf = os.path.join(package.folder_path, "temp.pdf")
        subprocess.run(["pdfunite", pdf_path, src, temp_pdf])
        os.remove(pdf_path)
        os.rename(temp_pdf, pdf_path)
    else:
        shutil.copyfile(src, pdf_path)

def evaluate_tests(package, pages):
    logger.info('Evaluate_tests...')
    tests = []
    DNNApi_instance = DNNApi.DNNApi()
    for i, page in enumerate(pages, start=1):
        logger.info('Processing page {}'.format(i))
        try:
            tests.append(evaluate_test(package, page, DNNApi_instance))
        except Exception as e:
            logging.warning(e)
    logger.info('Evaluate_tests... DONE')
    return tests


def evaluate_test(package, page, DNN):
    logger.info('Starting page processing')
    test = Test()
    img_container = ImageContainer()

    img_container.Name = find_non_exist_filename(package.folder_path, "jpg")
    img_container.img_path = os.path.join(package.folder_path, img_container.Name)
    img = np.array(page)

    cache = normalize_image(img)

    try:
        im_bgr = cv2.cvtColor(cache, cv2.COLOR_RGB2BGR)
        cv2.imwrite(img_container.img_path, im_bgr)

        img_container.answers = DNN.evaluate_answers(cache)
        test.test_group = DNN.evaluate_group(cache)
        test.student_index = DNN.evaluate_index(cache)

        del cache
    except Exception as e:
        tb = traceback.format_exc()
        logger.error('Failed to proceed page, reason: {}'.format(str(e)))
        logger.error(tb)

        im_bgr = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        cv2.imwrite(img_container.img_path, im_bgr)
        test.student_index = 'XXXXXX'
        img_container.answers = generate_empty_answers(50)  # TODO MS: may need fix in future

    test.images.append(img_container)

    try:
        appraise_test(test, package)
    except Exception as e:
        tb = traceback.format_exc()
        logger.error(
            'Failed to appraise page with index {}, reason: {}'.format(img_container.student_index, str(e)))
        logger.error(tb)

    save_test(package, test)
    logger.info('Processing page done')
    return test


def set_final_directory(package, data_directory=DATA_DIRECTORY):
    if os.path.exists(data_directory) is False:
        logger.warning("{} does NOT exist!!! creating...".format(data_directory))
        os.mkdir(data_directory)
    directory = find_empty_directory(package.Name, data_directory)
    os.mkdir(directory)
    package.folder_path = directory


def find_empty_directory(package_name, data_directory):
    path = os.path.join(data_directory, package_name + str(uuid.uuid4()))
    path = unidecode(path) #Removes non standard symbols, i.e. converts "ł" to "l"
    return path


def find_non_exist_filename(folder_path, extension):
    path = os.path.join(folder_path, NAME + str(uuid.uuid4()) + "." + extension)
    path = unidecode(path) #Removes non standard symbols, i.e. converts "ł" to "l"
    return path
