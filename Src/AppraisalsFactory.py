
def get_appraisal(name):
    if name == 'sum':
        return sum_appraisal
    elif name == 'basic':
        return basic_appraisal
    elif name == 'sum_double':
        return sum_appraisal_double


def sum_appraisal(expected, actual, allow_negative):
    return per_checkbox_appraisal(expected, actual, allow_negative, 1, -1)

def sum_appraisal_double(expected, actual, allow_negative):
    return per_checkbox_appraisal(expected, actual, allow_negative, 2, -1)

def per_checkbox_appraisal(expected, actual, allow_negative, correct_answer_points, wrong_answer_points):
    points = 0
    for character in actual:
        if character in expected:
            points += correct_answer_points
        else:
            points += wrong_answer_points
    if not allow_negative:
        points = max(0, points)
    return points


def basic_appraisal(expected, actual, allow_negative):
    if actual == expected:
        return 1
    else:
        return 0
