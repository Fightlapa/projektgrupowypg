import datetime
import logging

import sqlite3
from sqlite3 import Error

MODULE_NAME = "DataBaseController"
DATABASE_PATH = "db.sqlite3"
logger = logging.getLogger('exams')

def save_package(package, appraisal_name='basic', database_file=DATABASE_PATH):
    logger.info("Saving package to DataBase")
    connection = create_connection(database_file)
    cur = connection.cursor()
    cur.execute('''INSERT INTO main_package(folder_path, name, date_created, score_mode, status, allow_negative, error_message)
    VALUES (?,?,?,?,?,?,?)''', (package.folder_path, package.Name, datetime.datetime.now(), appraisal_name, 'processing', package.allow_negative, ''))
    package.id = cur.lastrowid
    connection.commit()
    connection.close()
    return package



def save_test(package, test, database_file=DATABASE_PATH):
    connection = create_connection(database_file)
    cur = connection.cursor()

    create_test(cur, package.id, test)
    connection.commit()
    connection.close()


def update_test(test, database_file=DATABASE_PATH):
    connection = create_connection(database_file)
    cur = connection.cursor()
    cur.execute('''UPDATE main_test set acquired_points = ?, student_index = ? WHERE id = ?''',
                (test.acquired_points, test.student_index, test.id))
    for image in test.images:
        cur.execute('''UPDATE main_imagecontainer set acquired_points = ? where id = ?''',
                    (image.acquired_points, image.id))
        for answer in image.answers:
            cur.execute(
                '''UPDATE main_answercontainer set acquired_points = ?, evaluated_value = ? WHERE id = ?''',
                (answer.acquired_points, answer.evaluated_value, answer.id))
    connection.commit()
    connection.close()


def create_tests(package, tests):
    connection = create_connection(DATABASE_PATH)
    cur = connection.cursor()
    for test in tests:
        create_test(cur, package.id, test)
    connection.commit()
    connection.close()


def create_test(cur, package_id, test):
    cur.execute('''INSERT INTO main_test(acquired_points, student_index, package_id, test_group, is_reference) VALUES (?,?,?,?,?)''',
                (test.acquired_points, test.student_index, package_id, test.test_group, False))
    test.id = cur.lastrowid
    for image in test.images:
        cur.execute('''INSERT INTO main_imagecontainer(acquired_points, img_path, test_id) VALUES (?,?,?)''',
                    (image.acquired_points, image.img_path, test.id))
        image_id = cur.lastrowid
        for answer in image.answers:
            cur.execute(
                '''INSERT INTO main_answercontainer(acquired_points, evaluated_value, answer_id, x1, x2, y1, y2, img_id) VALUES (?,?,?,?,?,?,?,?)''',
                (answer.acquired_points, answer.evaluated_value, answer.answer_id, answer.x1, answer.x2, answer.y1, answer.y2,
                 image_id))


def change_package_status(package, status, error_message= '', database_file=DATABASE_PATH):
    connection = create_connection(database_file)
    cur = connection.cursor()
    cur.execute('''UPDATE main_package set status = ? WHERE id = ?''',
                (status, package.id))
    if error_message != '':
        cur.execute('''UPDATE main_package set error_message = ? WHERE id = ?''',
                    (error_message, package.id))
    connection.commit()
    connection.close()


def change_test_to_ref_test(test, database_file=DATABASE_PATH):
    connection = create_connection(database_file)
    cur = connection.cursor()
    cur.execute('''UPDATE main_test set is_reference = ? WHERE id = ?''',
                (True, test.id))
    connection.commit()
    connection.close()


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None
