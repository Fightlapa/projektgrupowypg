import os

import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Activation
from keras.layers import Flatten
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
import keras.backend as K
from sklearn.model_selection import train_test_split
import datetime

def Clear_backend():
    keras.backend.clear_session()

# This is matric for exact match, it returns 1 only if all predictions matches true labels, if at least one answer is predicted wrongly with threshold 0.5, it will return 0
def macro_accuracy(y_true, y_pred):
    return K.min(K.cast(K.equal(y_true, K.round(y_pred)), dtype='float16'), axis=1)

dependencies = {
    'macro_accuracy': macro_accuracy
}

def resave_answer_CNN_to_full_model():
    required_height = 60
    required_width = 290
    image_shape = (required_height, required_width)

    model = CNN_answer(image_shape + (3,))
    model.load_weights(os.path.join(os.path.dirname(os.path.abspath(__file__)), "answer.weights"))
    model.save('./KerasModels/CNN_answer_full')

def CNN_answer_from_model():
    return keras.models.load_model('./KerasModels/CNN_answer_full', custom_objects=dependencies)

def resave_index_CNN_to_full_model():
    required_height = 290
    required_width = 60
    image_shape = (required_height, required_width)
    model = CNN_student_index(image_shape + (3,))
    model.load_weights(os.path.join(os.path.dirname(os.path.abspath(__file__)), "index.weights"))
    model.save('./KerasModels/CNN_student_index_full')

def CNN_student_index_from_model():
    return keras.models.load_model('./KerasModels/CNN_student_index_full', custom_objects=dependencies)


def CNN_answer(input_shape, lr=1e-5, epochs=800):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same',
                     input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(128, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(128, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(256, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(2048))
    model.add(Activation('relu'))
    model.add(Dropout(0.225))
    
    model.add(Dense(1024))
    model.add(Activation('relu'))
    model.add(Dropout(0.215))
    
    model.add(Dense(712))
    model.add(Activation('relu'))
    model.add(Dropout(0.205))
    model.add(Dense(4))
    model.add(Activation('sigmoid'))

    opt = keras.optimizers.Adam(lr=lr)
    # TODO: spróbuj tego:
    # decay = lr / epochs
    # momentum = 0.8
    #opt = keras.optimizers.SGD(lr=lr*10, momentum=momentum, decay=decay) #  może jeszcze nesterov=False

    model.compile(loss='binary_crossentropy',
                  optimizer=opt,
                  metrics=['binary_accuracy', macro_accuracy])
    return model


def CNN_student_index(input_shape, lr=1e-5, epochs=800):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same',
                     input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(128, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(128, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(256, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(1024))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))
    
    model.add(Dense(712))
    model.add(Activation('relu'))
    model.add(Dropout(0.225))
    model.add(Dense(11))
    model.add(Activation('softmax'))

    opt = keras.optimizers.Adam(lr=lr)
    # TODO: spróbuj tego:
    # decay = lr / epochs
    # momentum = 0.8
    #opt = keras.optimizers.SGD(lr=lr*10, momentum=momentum, decay=decay) #  może jeszcze nesterov=False

    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['binary_accuracy', macro_accuracy])
    return model


def train_model(training_data, input_shape, weights_filename, epochs=800, batch_size=8, test_size=0.2):
    # split to train and test data
    X_train, X_val, y_train, y_val = train_test_split(*training_data, test_size=test_size)

    # create train data generator
    datagen = ImageDataGenerator(
        featurewise_center=False,
        featurewise_std_normalization=False,
        rotation_range=3,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=False)
        #brightness_range=[0.8, 1.2])

    datagen.fit(X_train)

    # create test data generator
    test_datagen = ImageDataGenerator()

    test_datagen.fit(X_val)

    model = CNN_answer(input_shape, epochs)

    #model.load_weights(weights_filename)

    #tensorboard callbacks
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=0)

    model.fit_generator(datagen.flow(X_train, y_train, batch_size=batch_size),
                    steps_per_epoch=len(X_train) / batch_size,
                    epochs=epochs,
                    validation_data=test_datagen.flow(X_val, y_val, batch_size=batch_size),
                    shuffle=True,
                    callbacks=[tensorboard_callback])

    model.save_weights(weights_filename)

    print(model.evaluate(X, y))
