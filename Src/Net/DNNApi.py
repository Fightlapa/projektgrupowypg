import Src.Net.model as model
from Src.Preprocessing.data_extractor import get_index_column, extract_group_row, get_answer_row
import numpy as np
from tensorflow import Graph, Session

MODULE_NAME = "indexEvaluator"


class DNNApi:

    def __init__(self):
        self.graph_student_index = Graph()
        with self.graph_student_index.as_default():
            self.session_student_index = Session()
            with self.session_student_index.as_default():
                self.CNN_student_index_model = model.CNN_student_index_from_model()

        self.graph_answer = Graph()
        with self.graph_answer.as_default():
            self.session_answer = Session()
            with self.session_answer.as_default():
                self.CNN_answer_model = model.CNN_answer_from_model()

    def evaluate_index(self, img):
        # default parameters for neural network CNN_student_index_model
        required_height = 290
        required_width = 60
        index = ""
        # iterate over all columns
        with self.graph_student_index.as_default():
            with self.session_student_index.as_default():
                for img_index_column in get_index_column(img, required_height, required_width):
                    value = self.CNN_student_index_model.predict(img_index_column)
                    prediction = np.argmax(value)

                    # 0 means that no answer was selected
                    if prediction == 0:
                        prediction = "_"
                    else:
                        prediction = prediction % 10
                    index += str(prediction)

        return index

    def evaluate_answers(self, img):
        result = []
        threshold = 0.5

        with self.graph_answer.as_default():
            with self.session_answer.as_default():
                # iterate over all AnswerContainer class objects
                for answer_container, img in get_answer_row(img):
                    predict = self.CNN_answer_model.predict(img)

                    # set threshold
                    predict[predict >= threshold] = 1
                    predict[predict < threshold] = 0

                    # decode answer from vector to characters
                    answer = decode_answers(predict.flatten())

                    answer_container.evaluated_value = answer
                    result.append(answer_container)

        return result

    def evaluate_group(self, img):
        with self.graph_answer.as_default():
            with self.session_answer.as_default():
                predict = self.CNN_answer_model.predict(extract_group_row(img))

                prediction = np.argmax(predict)
                if np.take(predict, prediction) < 0.5:
                    return 'X'
                return str(chr(ord('A')+prediction))

def decode_answers(encoded):
    answer = ""

    for i, value in enumerate(encoded):
        if value == 1:
            answer += str(chr(ord('A')+i))

    return answer
